#![cfg(not(target_arch = "wasm32"))]

use std::{fs::File, io::BufWriter};

use clap::{App, Arg, SubCommand};
use query_segmenter::{Dictionary, Segmenter};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("Dictionary Builder")
        .version("0.1.0")
        .author("Tatsuya Kawano")
        .about("")
        .subcommand(
            SubCommand::with_name("build")
                .about("")
                .arg(
                    Arg::with_name("INPUT")
                        .help("Sets the input file to use")
                        .required(true)
                        .index(1),
                )
                .arg(
                    Arg::with_name("OUTPUT")
                        .help("Sets the output file to use")
                        .required(true)
                        .index(2),
                ),
        )
        .subcommand(
            SubCommand::with_name("split-words")
                .about("")
                .arg(
                    Arg::with_name("INPUT")
                        .help("Sets the input file to use")
                        .required(true)
                        .index(1),
                )
                .arg(Arg::with_name("QUERY").help("").required(true).index(2)),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("build") {
        let input = matches.value_of("INPUT").unwrap();
        let output = matches.value_of("OUTPUT").unwrap();

        let dict = Dictionary::create_from_elasticlunr_index(input)?;

        let mut writer = BufWriter::new(File::create(output)?);
        dict.save(&mut writer)?;

        println!(
            r#"Saved a dictionary with {} words to "{}""#,
            dict.word_count, output
        );
    } else if let Some(matches) = matches.subcommand_matches("split-words") {
        let input = matches.value_of("INPUT").unwrap();
        let query = matches.value_of("QUERY").unwrap();

        let f = std::fs::File::open(input)?;
        let mut reader = std::io::BufReader::new(f);
        let dict = Dictionary::load(&mut reader)?;
        println!(
            r#"Loaded a dictionary with {} words from "{}""#,
            dict.word_count, input
        );

        let segmenter = Segmenter::new(dict);
        let words = segmenter.split_words(&query);
        println!("{:?}", words);
    }

    Ok(())
}
