mod dictionary;
mod louds;
mod segmenter;
mod trie;

#[cfg(not(target_arch = "wasm32"))]
mod index_reader;
#[cfg(not(target_arch = "wasm32"))]
mod naive_trie;

pub use dictionary::Dictionary;
pub use segmenter::Segmenter;

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

#[cfg(target_arch = "wasm32")]
pub(crate) mod wasm_utils {
    use crate::Segmenter;

    use once_cell::sync::Lazy;
    use std::sync::Mutex;
    use wasm_bindgen::prelude::*;

    pub(crate) static SEGMENTER: Lazy<Mutex<Option<Segmenter>>> = Lazy::new(|| Mutex::new(None));

    #[wasm_bindgen]
    extern "C" {
        #[wasm_bindgen(js_namespace = console)]
        pub(crate) fn log(a: &str);
    }

    #[macro_export]
    macro_rules! console_log {
        ($($t:tt)*) => (wasm_utils::log(&format_args!($($t)*).to_string()))
    }

    pub(crate) fn performance() -> Result<web_sys::Performance, JsValue> {
        let window = web_sys::window()
            .ok_or_else(|| JsValue::from_str("should have a window in this context"))?;
        let performance = window
            .performance()
            .ok_or_else(|| JsValue::from_str("performance should be available"))?;
        Ok(performance)
    }
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn loadDict(serialized_dict: &[u8]) -> Result<(), JsValue> {
    let performance = wasm_utils::performance()?;
    let start = performance.now();

    if wasm_utils::SEGMENTER.lock().unwrap().is_some() {
        let duration = performance.now() - start;
        console_log!("initDict() took {:.3} ms. (Already initialized)", duration);
        return Ok(());
    }

    let mut reader = std::io::Cursor::new(serialized_dict);
    let (resp, word_count) = match Dictionary::load(&mut reader) {
        Ok(dict) => {
            let word_count = dict.word_count;
            *wasm_utils::SEGMENTER.lock().unwrap() = Some(Segmenter::new(dict));
            (Ok(()), word_count)
        }
        Err(e) => (Err(JsValue::from_str(&e.to_string())), 0),
    };

    let duration = performance.now() - start;
    console_log!("initDict() took {:.3} ms. ({} words)", duration, word_count);

    resp
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn splitWords(query: &str) -> Result<String, JsValue> {
    let query_len = query.chars().count();
    let performance = wasm_utils::performance()?;
    let start = performance.now();

    let resp = if let Some(segmenter) = &*wasm_utils::SEGMENTER.lock().unwrap() {
        Ok(segmenter.split_words(query).join(" "))
    } else {
        Err(JsValue::from_str(
            "Please call load_dict() before calling split_words()",
        ))
    };

    let duration = performance.now() - start;
    console_log!(
        "splitWords() took {:.3} ms. (Query: {} chars)",
        duration,
        query_len
    );

    resp
}
