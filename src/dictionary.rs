use crate::trie::Trie;

#[cfg(not(target_arch = "wasm32"))]
use crate::{index_reader::Index, trie::builder::TrieBuilder};

pub struct Dictionary {
    pub word_count: u32,
    pub(crate) trie: Trie,
}

#[cfg(not(target_arch = "wasm32"))]
impl Dictionary {
    pub fn create_from_elasticlunr_index<P: AsRef<std::path::Path>>(
        path: P,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let index = Index::from_path(path)?;
        let words = index.collect_words();
        let word_count = words.len() as u32;
        let mut trie_builder = TrieBuilder::default();

        for word in words {
            trie_builder.insert(&word)?;
        }

        Ok(Dictionary {
            trie: trie_builder.build(),
            word_count,
        })
    }

    pub fn save<W: std::io::Write>(
        &self,
        writer: &mut W,
    ) -> Result<(), Box<dyn std::error::Error>> {
        writer.write_all(&self.word_count.to_le_bytes())?;
        self.trie.serialize(writer)?;
        writer.flush()?;
        Ok(())
    }
}

impl Dictionary {
    pub fn load<R: std::io::Read>(reader: &mut R) -> Result<Self, Box<dyn std::error::Error>> {
        let mut buf = [0; 4];
        reader.read_exact(&mut buf)?;
        let word_count = u32::from_le_bytes(buf);
        let trie = Trie::deserialize(reader)?;
        Ok(Self { trie, word_count })
    }
}
