use crate::louds::Louds;

use std::ops::RangeInclusive;

#[cfg(not(target_arch = "wasm32"))]
pub(crate) mod builder {
    use super::{Label, Trie};
    use crate::{
        louds::builder::LoudsBuilder,
        naive_trie::{Element, NaiveTrie},
    };

    #[derive(Default)]
    pub(crate) struct TrieBuilder {
        naive_trie: NaiveTrie,
    }

    impl TrieBuilder {
        // TODO: Define a custom error.
        pub(crate) fn insert(&mut self, word: &str) -> Result<(), Box<dyn std::error::Error>> {
            self.naive_trie.insert(word)
        }

        pub(crate) fn build(self) -> Trie {
            let mut louds_builder = LoudsBuilder::default();
            let mut labels = Vec::default();

            louds_builder.push(true);
            louds_builder.push(false);

            for node in &self.naive_trie {
                match node {
                    Element::Node { label, is_terminal } => {
                        louds_builder.push(true);
                        labels.push(Label::new(label, is_terminal))
                    }
                    Element::NoMoreChild => {
                        louds_builder.push(false);
                    }
                }
            }

            Trie {
                louds: louds_builder.build(),
                labels: labels.into_boxed_slice(),
                max_word_len: self.naive_trie.max_word_len,
            }
        }
    }
}

#[derive(PartialEq, Eq)]
pub(crate) struct Trie {
    louds: Louds,         // Node ID 0 is unused. The root node has node ID 1.
    labels: Box<[Label]>, // labels[0] holds the label for LOUDS node with ID 2.
    max_word_len: u8,
}

macro_rules! unwrap_or_break {
    ($e: expr) => {
        if let Some(v) = $e {
            v
        } else {
            break;
        }
    };
}

impl Trie {
    pub(crate) fn common_prefix_search(&self, query: &str) -> Vec<String> {
        let mut words = Vec::default();
        let mut substr = String::default();
        let mut node_id = 1;

        let get_label_value = |id| self.label(id).unwrap().value();

        for c in query.chars() {
            let (num_children, mut child_ids) = unwrap_or_break!(self.louds.children(node_id));
            let maybe_child_id = if num_children < 16 {
                // If the range of the node IDs is small enough, do a linear search.
                child_ids.find(|id| get_label_value(*id) == c)
            } else {
                // Otherwise, do a binary search.
                binary_search(&child_ids, &get_label_value, c)
            };
            let child_id = unwrap_or_break!(maybe_child_id);

            substr.push(c);

            if self.label(child_id).unwrap().is_terminal() {
                words.push(substr.clone());
            };

            node_id = child_id;
        }
        words
    }

    pub(crate) fn max_word_len(&self) -> u8 {
        self.max_word_len
    }

    fn label(&self, node_id: u32) -> Option<&Label> {
        if node_id < 2 || node_id > self.louds.bit_len() {
            None
        } else {
            self.labels.get(node_id as usize - 2)
        }
    }
}

fn binary_search<F>(range: &RangeInclusive<u32>, get_label_value: &F, c: char) -> Option<u32>
where
    F: Fn(u32) -> char,
{
    use std::cmp::Ordering::*;
    let mut start = *range.start();
    let mut end = *range.end();
    while end - start > 1 {
        let mid = (end + start) / 2;
        match get_label_value(mid).cmp(&c) {
            Equal => return Some(mid),
            Greater => end = mid,
            Less => start = mid,
        }
    }
    None
}

// Serializer
#[cfg(not(target_arch = "wasm32"))]
impl Trie {
    pub(crate) fn serialize<W: std::io::Write>(
        &self,
        writer: &mut W,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // TODO: Make the serialized format more robust against transmission errors, etc.
        // e.g. We could add a header with the magic and serialize version, and a trailer
        // with a checksum or something.
        self.louds.serialize(writer)?;
        let labels_len = self.labels.len() as u32; // TODO: Maybe u64?
        writer.write_all(&labels_len.to_le_bytes())?;
        for label in self.labels.iter() {
            label.serialize(writer)?;
        }
        writer.write_all(&[self.max_word_len])?;
        Ok(())
    }
}

// Deserializer
impl Trie {
    pub(crate) fn deserialize<R: std::io::Read>(
        reader: &mut R,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let louds = Louds::deserialize(reader)?;
        let mut buf = [0; 4];
        reader.read_exact(&mut buf)?;
        let labels_len = u32::from_le_bytes(buf) as usize;
        let mut labels = Vec::with_capacity(labels_len);
        for _ in 0..labels_len {
            labels.push(Label::deserialize(reader)?);
        }
        let mut buf = [0; 1];
        reader.read_exact(&mut buf)?;
        let max_word_len = buf[0];

        Ok(Self {
            louds,
            labels: labels.into_boxed_slice(),
            max_word_len,
        })
    }
}

#[cfg(not(target_arch = "wasm32"))]
impl std::fmt::Debug for Trie {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.louds.bit_len() == 0 {
            return f.write_str("empty");
        }

        const MAX_NODES: u32 = 1_000;

        let mut node_id = 0;
        f.write_str("{")?;

        for i in 0..self.louds.bit_len() {
            let b = self.louds[i];
            f.write_str(if b { "1" } else { "0" })?;

            if !b {
                f.write_str("}")?;
                node_id += 1;

                if i < self.louds.bit_len() - 1 {
                    let label = if let Some(lb) = self.label(node_id) {
                        lb.value()
                    } else {
                        '\u{0}'
                    };
                    write!(f, " {}:{}{{", node_id, label)?;
                }

                if node_id > MAX_NODES {
                    break;
                }
            }
        }
        Ok(())
    }
}

// The mask used to pack is_terminal flag with a char (a 32-bit value but only
// lower 21 bits are used for Unicode scalar)
const TERM_FLAG_MASK: u32 = 0x8000_0000;

#[derive(PartialEq, Eq)]
struct Label(u32);

impl Label {
    #[cfg(not(target_arch = "wasm32"))]
    fn new(value: char, is_terminal: bool) -> Self {
        if is_terminal {
            Self(value as u32 | TERM_FLAG_MASK)
        } else {
            Self(value as u32)
        }
    }

    fn value(&self) -> char {
        std::char::from_u32(self.0 & !TERM_FLAG_MASK).unwrap()
    }

    fn is_terminal(&self) -> bool {
        self.0 & TERM_FLAG_MASK != 0
    }
}

// Serializer
#[cfg(not(target_arch = "wasm32"))]
impl Label {
    pub(crate) fn serialize<W: std::io::Write>(
        &self,
        writer: &mut W,
    ) -> Result<(), Box<dyn std::error::Error>> {
        writer.write_all(&self.0.to_le_bytes())?;
        Ok(())
    }
}

// Deserializer
impl Label {
    pub(crate) fn deserialize<R: std::io::Read>(
        reader: &mut R,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let mut buf = [0; 4];
        reader.read_exact(&mut buf)?;
        let val = u32::from_le_bytes(buf);
        Ok(Self(val))
    }
}

#[cfg(not(target_arch = "wasm32"))]
impl std::fmt::Debug for Label {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.value().to_string())?;
        if self.is_terminal() {
            f.write_str(" (terminal)")?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{builder::TrieBuilder, Trie};

    #[test]
    fn common_prefix_search_e() -> Result<(), Box<dyn std::error::Error>> {
        let mut builder = TrieBuilder::default();
        builder.insert("a")?;
        builder.insert("apple")?;
        builder.insert("app")?;
        builder.insert("application")?;

        let trie = builder.build();

        // max_word_len is not a number of bytes in UTF-8, but a number of chars (Unicode scalars)
        assert_eq!(trie.max_word_len(), "application".chars().count() as u8);

        assert_eq!(trie.common_prefix_search("app"), &["a", "app"]);
        assert_eq!(trie.common_prefix_search("apples"), &["a", "app", "apple"]);
        assert_eq!(
            trie.common_prefix_search("applications"),
            &["a", "app", "application"]
        );
        assert!(trie.common_prefix_search("oranges").is_empty());

        Ok(())
    }

    #[test]
    fn common_prefix_search_j() -> Result<(), Box<dyn std::error::Error>> {
        let mut builder = TrieBuilder::default();
        builder.insert("トレイト")?;
        builder.insert("境界")?;
        builder.insert("トレイトメソッド")?;
        builder.insert("呼び出し")?;
        builder.insert("トレース")?;
        builder.insert("メソッド")?;

        let trie = builder.build();

        // max_word_len is not a number of bytes in UTF-8, but a number of chars (Unicode scalars)
        assert_eq!(
            trie.max_word_len(),
            "トレイトメソッド".chars().count() as u8
        );

        assert_eq!(
            trie.common_prefix_search("トレイトメソッド"),
            &["トレイト", "トレイトメソッド"]
        );

        assert_eq!(trie.common_prefix_search("境界線"), &["境界"]);
        assert!(trie.common_prefix_search("関数").is_empty());

        let base_query = "トレイトのメソッドを呼び出します"
            .chars()
            .collect::<Vec<_>>();

        // e.g.
        // make_query(0) -> "トレイトのメソッ"
        // make_query(1) -> "レイトのメソッド"
        // make_query(2) -> "イトのメソッドを"
        let make_query = |pos: usize| -> String {
            let len = trie.max_word_len() as usize;
            let end_pos = (pos + len).min(base_query.len());
            base_query[pos..end_pos].iter().collect()
        };

        assert_eq!(trie.common_prefix_search(&make_query(0)), &["トレイト"]);
        assert_eq!(trie.common_prefix_search(&make_query(5)), &["メソッド"]);
        assert_eq!(trie.common_prefix_search(&make_query(10)), &["呼び出し"]);

        assert!(trie.common_prefix_search(&make_query(1)).is_empty());
        assert!(trie.common_prefix_search(&make_query(6)).is_empty());

        Ok(())
    }

    #[test]
    fn serialize_deserialize() -> Result<(), Box<dyn std::error::Error>> {
        let mut builder = TrieBuilder::default();
        builder.insert("トレイト")?;
        builder.insert("境界")?;
        builder.insert("トレイトメソッド")?;
        builder.insert("呼び出し")?;
        builder.insert("トレース")?;
        builder.insert("メソッド")?;

        let trie1 = builder.build();

        let mut binary = Vec::default();
        trie1.serialize(&mut binary)?;

        let mut reader = std::io::Cursor::new(binary);
        let trie2 = Trie::deserialize(&mut reader)?;

        assert_eq!(trie1, trie2);
        Ok(())
    }
}
