use std::collections::{BTreeMap, VecDeque};

#[derive(Default)]
pub(crate) struct NaiveTrie {
    root: NaiveTrieNode,
    pub(crate) max_word_len: u8,
}

impl NaiveTrie {
    pub(crate) fn insert(&mut self, word: &str) -> Result<(), Box<dyn std::error::Error>> {
        if word.is_empty() {
            return Ok(());
        }

        let word_len = word.chars().count();
        if word_len > u8::max_value() as usize {
            return Err(format!("Word has too many characters: {}", word_len).into());
        } else {
            self.max_word_len = self.max_word_len.max(word_len as u8);
        }

        let mut node = &mut self.root;
        for c in word.chars() {
            node = node
                .children
                .entry(c)
                .or_insert_with(|| NaiveTrieNode::new(c));
        }
        node.is_terminal = true;
        Ok(())
    }
}

impl<'a> IntoIterator for &'a NaiveTrie {
    type Item = Element;
    type IntoIter = NaiveTrieIter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        NaiveTrieIter::new(&self)
    }
}

struct NaiveTrieNode {
    children: BTreeMap<char, NaiveTrieNode>,
    label: char,
    is_terminal: bool,
}

impl Default for NaiveTrieNode {
    fn default() -> Self {
        Self {
            children: BTreeMap::default(),
            label: '\u{0}',
            is_terminal: false,
        }
    }
}

impl NaiveTrieNode {
    fn new(label: char) -> Self {
        Self {
            children: BTreeMap::default(),
            label,
            is_terminal: false,
        }
    }
}

pub(crate) struct NaiveTrieIter<'a> {
    unvisited: VecDeque<InternalElement<'a>>,
}

impl<'a> NaiveTrieIter<'a> {
    fn new(trie: &'a NaiveTrie) -> Self {
        let mut unvisited = VecDeque::default();
        for child in trie.root.children.values() {
            unvisited.push_back(InternalElement::Node(child));
        }
        unvisited.push_back(InternalElement::NoMoreChild);
        Self { unvisited }
    }
}

pub(crate) enum Element {
    NoMoreChild,
    Node { label: char, is_terminal: bool },
}

enum InternalElement<'a> {
    NoMoreChild,
    Node(&'a NaiveTrieNode),
}

impl<'a> Iterator for NaiveTrieIter<'a> {
    type Item = Element;

    fn next(&mut self) -> Option<Self::Item> {
        self.unvisited.pop_front().map(|element| match element {
            InternalElement::Node(node) => {
                for child in node.children.values() {
                    self.unvisited.push_back(InternalElement::Node(child));
                }
                self.unvisited.push_back(InternalElement::NoMoreChild);
                Element::Node {
                    label: node.label,
                    is_terminal: node.is_terminal,
                }
            }
            InternalElement::NoMoreChild => Element::NoMoreChild,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::NaiveTrie;

    #[test]
    fn tree_structure() -> Result<(), Box<dyn std::error::Error>> {
        let mut trie = NaiveTrie::default();
        trie.insert("a")?;
        trie.insert("app")?;
        trie.insert("application")?;
        trie.insert("apple")?;

        assert_eq!(trie.root.children.len(), 1);
        assert!(!trie.root.is_terminal);
        let subtree = trie.root.children.get(&'a').unwrap();
        assert_eq!(subtree.children.len(), 1);
        assert!(subtree.is_terminal);

        Ok(())
    }
}
