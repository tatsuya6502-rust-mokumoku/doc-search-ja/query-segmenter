use std::ops::{Index, RangeInclusive};

const LARGE_BLOCK_BITS: u32 = 1_024;
const SMALL_BLOCK_BITS: u32 = 32;

#[cfg(not(target_arch = "wasm32"))]
pub(crate) mod builder {
    use super::Louds;

    pub(crate) struct LoudsBuilder {
        lbs: Vec<u8>,
        last_byte: u8,
        bit_pos: u8,
    }

    impl Default for LoudsBuilder {
        fn default() -> Self {
            Self {
                lbs: Vec::default(),
                last_byte: 0,
                bit_pos: 7,
            }
        }
    }

    impl LoudsBuilder {
        // TODO: Check the max size.
        pub(crate) fn push(&mut self, value: bool) {
            if value {
                self.last_byte |= 1 << self.bit_pos;
            }
            if self.bit_pos == 0 {
                self.lbs.push(self.last_byte);
                self.last_byte = 0;
                self.bit_pos = 7;
            } else {
                self.bit_pos -= 1;
            }
        }

        pub(crate) fn build(mut self) -> Louds {
            let mut bit_len = (self.lbs.len() * 8) as u32;
            if self.bit_pos < 7 {
                self.lbs.push(self.last_byte);
                bit_len += 7 - self.bit_pos as u32;
            }
            Louds::new(self.lbs, bit_len)
        }
    }
}

#[derive(PartialEq, Eq)]
pub(crate) struct Louds {
    bit_len: u32,
    max_pop_count: u32,
    lbs: Box<[u8]>,
    index: Box<[LargeBlock]>,
}

#[cfg(not(target_arch = "wasm32"))]
impl Louds {
    fn new(lbs: Vec<u8>, bit_len: u32) -> Self {
        let (index, max_pop_count) = Self::build_index(&lbs);
        Self {
            bit_len,
            max_pop_count,
            lbs: lbs.into_boxed_slice(),
            index,
        }
    }

    fn build_index(lbs: &[u8]) -> (Box<[LargeBlock]>, u32) {
        let index_len = ((lbs.len() as f64) / ((LARGE_BLOCK_BITS / 8) as f64)).ceil() as usize;
        let mut index = Vec::with_capacity(index_len);
        let mut pop_count = 0;
        for block in lbs.chunks((LARGE_BLOCK_BITS / 8) as usize) {
            let (lb, count) = LargeBlock::new(pop_count, &block);
            index.push(lb);
            pop_count = count;
        }
        (index.into_boxed_slice(), pop_count)
    }
}

impl Louds {
    pub(crate) fn bit_len(&self) -> u32 {
        self.bit_len
    }

    pub(crate) fn children(&self, node_id: u32) -> Option<(u32, RangeInclusive<u32>)> {
        self.select0(node_id).and_then(|pos| {
            let start = pos + 1;
            if let Some(end) = (start..).take_while(|id| self[*id]).last() {
                let start_id = self.rank1(start);
                let end_id = start_id + (end - start);
                return Some((end - start + 1, start_id..=end_id));
            }
            None
        })
    }

    fn rank1(&self, pos: u32) -> u32 {
        self.rank(pos, true)
    }

    fn rank(&self, pos: u32, value: bool) -> u32 {
        let pos = (pos + 1).min(self.bit_len);

        let large_index = pos / LARGE_BLOCK_BITS;
        let small_index = (pos - large_index * LARGE_BLOCK_BITS) / SMALL_BLOCK_BITS;
        let lb = &self.index[large_index as usize];
        let mut pop_count = lb.pop_count + lb.small_blocks[small_index as usize] as u32;

        let remaining_bits = (pos % SMALL_BLOCK_BITS) as usize;
        if remaining_bits > 0 {
            let offset = (pos as usize - remaining_bits) / 8;
            let num_bytes = remaining_bits / 8;
            pop_count += self.lbs[offset..(offset + num_bytes)]
                .iter()
                .map(|b| b.count_ones())
                .sum::<u32>();
            let remaining_bits = (remaining_bits % 8) as u8;
            if remaining_bits > 0 {
                pop_count += self.count_ones(offset + num_bytes, remaining_bits);
            }
        }

        if value {
            pop_count
        } else {
            pos - pop_count
        }
    }

    fn select0(&self, count: u32) -> Option<u32> {
        // Shortcut: We use Louds in Trie so the first two bits of the lbs will always be 1 and 0.
        if count == 1 && self[0] && !self[1] {
            Some(1)
        } else {
            self.select(count, false)
        }
    }

    fn select(&self, count: u32, value: bool) -> Option<u32> {
        if count == 0 {
            return Some(0);
        }

        let max_count = if value {
            self.max_pop_count
        } else {
            self.bit_len - self.max_pop_count
        };

        if count > max_count {
            return None;
        }

        // Run binary search
        let mut start = 0;
        let mut end = self.bit_len - 1;
        while end - start > 1 {
            let mid = (end + start) / 2;
            if self.rank(mid, value) >= count {
                end = mid;
            } else {
                start = mid;
            }
        }
        Some(end)
    }

    fn count_ones(&self, offset: usize, bits: u8) -> u32 {
        let byte = self.lbs[offset];
        let mask = match bits {
            1 => 0b1000_0000,
            2 => 0b1100_0000,
            3 => 0b1110_0000,
            4 => 0b1111_0000,
            5 => 0b1111_1000,
            6 => 0b1111_1100,
            7 => 0b1111_1110,
            _ => unreachable!(),
        };
        (byte & mask).count_ones()
    }
}

// Serializer
#[cfg(not(target_arch = "wasm32"))]
impl Louds {
    pub(crate) fn serialize<W: std::io::Write>(
        &self,
        writer: &mut W,
    ) -> Result<(), Box<dyn std::error::Error>> {
        writer.write_all(&self.bit_len.to_le_bytes())?;
        writer.write_all(&self.max_pop_count.to_le_bytes())?;
        let len = self.lbs.len() as u32; // TODO: Use u64?
        writer.write_all(&len.to_le_bytes())?;
        writer.write_all(&self.lbs)?;
        let len = self.index.len() as u32; // TODO: Use u64?
        writer.write_all(&len.to_le_bytes())?;
        for lb in self.index.iter() {
            lb.serialize(writer)?;
        }
        Ok(())
    }
}

// Deserializer
impl Louds {
    pub(crate) fn deserialize<R: std::io::Read>(
        reader: &mut R,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let mut buf = [0; 4];
        reader.read_exact(&mut buf)?;
        let bit_len = u32::from_le_bytes(buf);
        reader.read_exact(&mut buf)?;
        let max_pop_count = u32::from_le_bytes(buf);
        reader.read_exact(&mut buf)?;
        let len = u32::from_le_bytes(buf) as usize;
        let mut lbs = vec![0u8; len].into_boxed_slice();
        reader.read_exact(&mut lbs)?;
        reader.read_exact(&mut buf)?;
        let len = u32::from_le_bytes(buf) as usize;
        let mut index = Vec::with_capacity(len);
        for _ in 0..len {
            index.push(LargeBlock::deserialize(reader)?);
        }
        Ok(Self {
            bit_len,
            max_pop_count,
            lbs,
            index: index.into_boxed_slice(),
        })
    }
}

static TRUE: bool = true;
static FALSE: bool = false;

impl Index<u32> for Louds {
    type Output = bool;

    fn index(&self, index: u32) -> &Self::Output {
        let index = index as usize;
        let byte = self.lbs[index / 8];
        let bit = index % 8;

        let v = match bit {
            0 => byte & 0b1000_0000 != 0,
            1 => byte & 0b0100_0000 != 0,
            2 => byte & 0b0010_0000 != 0,
            3 => byte & 0b0001_0000 != 0,
            4 => byte & 0b0000_1000 != 0,
            5 => byte & 0b0000_0100 != 0,
            6 => byte & 0b0000_0010 != 0,
            7 => byte & 0b0000_0001 != 0,
            _ => unreachable!(),
        };

        if v {
            &TRUE
        } else {
            &FALSE
        }
    }
}

// Large block: 1,024 bits
// Small block:    32 bits
#[derive(Clone, Default, PartialEq, Eq)]
struct LargeBlock {
    pop_count: u32,
    small_blocks: [u16; 32], // pop counts for small blocks
}

#[cfg(not(target_arch = "wasm32"))]
impl std::fmt::Debug for LargeBlock {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{:?} ", self.pop_count, self.small_blocks)
    }
}

#[cfg(not(target_arch = "wasm32"))]
impl LargeBlock {
    fn new(pop_count: u32, lbs: &[u8]) -> (Self, u32) {
        debug_assert!(lbs.len() <= 128);
        let mut small_blocks = [0; 32];
        let mut small_pc = 0; // pop count for a small block.
        for (i, chunk) in lbs.chunks(4).enumerate() {
            small_pc += chunk.iter().map(|b| b.count_ones() as u16).sum::<u16>();
            if i <= 30 {
                small_blocks[i + 1] = small_pc;
            }
        }
        let lb = Self {
            pop_count,
            small_blocks,
        };
        (lb, pop_count + small_pc as u32)
    }
}

// Serializer
#[cfg(not(target_arch = "wasm32"))]
impl LargeBlock {
    pub(crate) fn serialize<W: std::io::Write>(
        &self,
        writer: &mut W,
    ) -> Result<(), Box<dyn std::error::Error>> {
        writer.write_all(&self.pop_count.to_le_bytes())?;
        for small_block in &self.small_blocks {
            writer.write_all(&small_block.to_le_bytes())?;
        }
        Ok(())
    }
}

// Deserializer
impl LargeBlock {
    pub(crate) fn deserialize<R: std::io::Read>(
        reader: &mut R,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let mut buf_u32 = [0; 4];
        reader.read_exact(&mut buf_u32)?;
        let pop_count = u32::from_le_bytes(buf_u32);
        let mut buf_u16 = [0; 2];
        let mut small_blocks = [0u16; 32];
        for small_block in &mut small_blocks {
            reader.read_exact(&mut buf_u16)?;
            *small_block = u16::from_le_bytes(buf_u16);
        }
        Ok(Self {
            pop_count,
            small_blocks,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::builder::LoudsBuilder;

    #[test]
    fn rank_and_select() {
        let mut builder = LoudsBuilder::default();
        builder.push(true); //  0
        builder.push(false); //  1
        builder.push(true); //  2
        builder.push(true); //  3
        builder.push(true); //  4
        builder.push(false); //  5
        builder.push(true); //  6
        builder.push(true); //  7
        builder.push(false); //  8
        builder.push(false); //  9
        builder.push(true); // 10
        builder.push(false); // 11
        builder.push(false); // 12
        builder.push(false); // 13
        builder.push(false); // 14

        let louds = builder.build();
        assert_eq!(louds.bit_len, 15);

        assert_eq!(louds.rank1(0), 1);
        assert_eq!(louds.rank1(1), 1);
        assert_eq!(louds.rank1(2), 2);
        assert_eq!(louds.rank1(3), 3);
        assert_eq!(louds.rank1(4), 4);
        assert_eq!(louds.rank1(5), 4);
        assert_eq!(louds.rank1(6), 5);
        assert_eq!(louds.rank1(7), 6);
        assert_eq!(louds.rank1(8), 6);
        assert_eq!(louds.rank1(9), 6);
        assert_eq!(louds.rank1(10), 7);
        assert_eq!(louds.rank1(11), 7);
        assert_eq!(louds.rank1(12), 7);
        assert_eq!(louds.rank1(13), 7);
        assert_eq!(louds.rank1(14), 7);
        assert_eq!(louds.rank1(15), 7);

        assert_eq!(louds.select0(0), Some(0));
        assert_eq!(louds.select0(1), Some(1));
        assert_eq!(louds.select0(2), Some(5));
        assert_eq!(louds.select0(3), Some(8));
        assert_eq!(louds.select0(4), Some(9));
        assert_eq!(louds.select0(5), Some(11));
        assert_eq!(louds.select0(6), Some(12));
        assert_eq!(louds.select0(7), Some(13));
        assert_eq!(louds.select0(8), Some(14));
        assert_eq!(louds.select0(9), None);
    }

    #[test]
    fn tree_traverse() {
        let mut builder = LoudsBuilder::default();
        builder.push(true); //  0
        builder.push(false); //  1
        builder.push(true); //  2
        builder.push(true); //  3
        builder.push(true); //  4
        builder.push(false); //  5
        builder.push(true); //  6
        builder.push(true); //  7
        builder.push(false); //  8
        builder.push(false); //  9
        builder.push(true); // 10
        builder.push(false); // 11
        builder.push(false); // 12
        builder.push(false); // 13
        builder.push(false); // 14

        let louds = builder.build();

        assert_eq!(louds.children(1), Some((3, 2..=4)));
        assert_eq!(louds.children(2), Some((2, 5..=6)));
        assert_eq!(louds.children(3), None);
        assert_eq!(louds.children(4), Some((1, 7..=7)));
        assert_eq!(louds.children(5), None);
        assert_eq!(louds.children(6), None);
        assert_eq!(louds.children(7), None);
    }

    #[test]
    fn index_lookup() {
        let mut builder = LoudsBuilder::default();
        for i in 1..=2049 {
            // true, true, false, true, true, false, ...
            builder.push(i % 3 != 0);
        }
        let louds = builder.build();

        assert_eq!(louds.bit_len, 2049);

        // expectation
        let ex = |x: u32| -> u32 {
            let x1 = x + 1;
            x1 - x1 / 3
        };

        assert_eq!(louds.rank1(0), ex(0));
        assert_eq!(louds.rank1(1), ex(1));
        assert_eq!(louds.rank1(2), ex(2));
        assert_eq!(louds.rank1(3), ex(3));
        assert_eq!(louds.rank1(6), ex(6));
        assert_eq!(louds.rank1(7), ex(7));
        assert_eq!(louds.rank1(15), ex(15));
        assert_eq!(louds.rank1(23), ex(23));
        assert_eq!(louds.rank1(31), ex(31));
        assert_eq!(louds.rank1(63), ex(63));
        assert_eq!(louds.rank1(126), ex(126));
        assert_eq!(louds.rank1(127), ex(127));
        assert_eq!(louds.rank1(128), ex(128));
        assert_eq!(louds.rank1(1023), ex(1023));
        assert_eq!(louds.rank1(2047), ex(2047));
        assert_eq!(louds.rank1(2048), ex(2048));
        // Out of the range. Expect the same value returned by rank1(2048).
        assert_eq!(louds.rank1(2049), ex(2048));
    }
}
