use serde_derive::Deserialize;
use std::collections::{BTreeMap, BTreeSet};

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Index {
    index: InnerIndex,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub(crate) struct InnerIndex {
    // fields: Vec<String>,
    // pipeline: Pipeline,
    // #[serde(rename = "ref")]
    // ref_field: String,
    version: String,
    index: BTreeMap<String, InvertedIndex>,
    document_store: DocumentStore,
    // lang: Language,
}

impl Index {
    pub(crate) fn from_path<P: AsRef<std::path::Path>>(
        path: P,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let f = std::fs::File::open(path)?;
        let mut reader = std::io::BufReader::new(f);
        let index: Self = serde_json::from_reader(&mut reader)?;
        Ok(index)
    }

    pub(crate) fn collect_words(&self) -> BTreeSet<String> {
        let mut words = BTreeSet::default();
        for inverted_index in self.index.index.values() {
            inverted_index.collect_words(&mut words);
        }
        words
    }
}

#[derive(Deserialize, Debug, PartialEq, Default)]
struct InvertedIndex {
    root: IndexItem,
}

impl InvertedIndex {
    fn collect_words(&self, words: &mut BTreeSet<String>) {
        let mut word = String::default();
        self.root.collect_words(&mut word, words);
    }
}

#[derive(Deserialize, Debug, Clone, PartialEq, Default)]
struct IndexItem {
    docs: BTreeMap<String, TermFrequency>,
    #[serde(rename = "df")]
    doc_freq: i64,
    #[serde(flatten)]
    children: BTreeMap<char, IndexItem>,
}

impl IndexItem {
    fn collect_words(&self, word: &mut String, words: &mut BTreeSet<String>) {
        if self.doc_freq > 0 {
            words.insert(word.clone());
        }

        for (c, index_item) in &self.children {
            word.push(*c);
            index_item.collect_words(word, words);
            word.pop();
        }
    }
}

#[derive(Debug, Copy, Clone, Deserialize, PartialEq)]
struct TermFrequency {
    #[serde(rename = "tf")]
    term_freq: f64,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct DocumentStore {
    save: bool,
    // docs: BTreeMap<String, BTreeMap<String, String>>,
    // doc_info: BTreeMap<String, BTreeMap<String, usize>>,
}
