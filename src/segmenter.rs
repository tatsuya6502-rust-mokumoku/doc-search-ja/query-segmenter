use crate::Dictionary;

use std::collections::BTreeSet;

pub struct Segmenter {
    dict: Dictionary,
}

impl Segmenter {
    pub fn new(dict: Dictionary) -> Self {
        Self { dict }
    }

    pub fn split_words(&self, query: &str) -> Vec<String> {
        if query.is_empty() {
            return Vec::default();
        }

        let mut char_indices = query.char_indices().map(|(i, _)| i).collect::<Vec<_>>();
        char_indices.push(query.len());

        let mut result = BTreeSet::default();

        let trie = &self.dict.trie;
        let max_len = trie.max_word_len() as usize;
        let query_len_1 = char_indices.len() - 1;

        // Note: We cannot use `slice::windows(n)` here because it always returns a slice with length n.
        // e.g. When the slice is [0, 1, 2, 3] and n = 3, `windows()` returns [0, 1, 2] and [1, 2, 3].
        // However we want [0, 1, 2], [1, 2, 3], [2, 3] and [3].
        for (i, start) in char_indices.iter().take(query_len_1).enumerate() {
            let end = char_indices[(i + max_len).min(query_len_1)];
            let words = trie.common_prefix_search(&query[*start..end]);
            for word in words {
                result.insert(word);
            }
        }

        result.into_iter().collect()
    }
}
