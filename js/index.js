"use strict"

// Returns a promise of the segmenter wasm module being downloaded and initialized.
async function initSegmenter() {
    let module = await import("../pkg/index.js");
    // let module = await import("../pkg/query_segmenter_bg.js");
    let response = await fetch('../dict-ja.bin');
    let bytes = await response.arrayBuffer();
    module.loadDict(new Uint8Array(bytes));
    return module;
    // catch(console.error);
}

(function() {
    window.segmenterLib = window.segmenterLib　|| {};

    var segmenter = initSegmenter();
    window.segmenterLib.segmenter = segmenter;
})();
