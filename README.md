# Query Segmenter

## Usage

### Building the Dictionary Builder

```console
$ cargo build --release
```

### Running the Dictionary Builder

**Creating the Dictionary**

```
$ cargo run --release -- build /path/to/searchindex.json ./static/dict-ja.bin
```

**Querying to the Dictionary**

```
$ cargo run --release -- split-words ./static/dict-ja.bin "検索キーワード"
```

### Building the Wasm Module

NOTE: If you are using M1 Mac, please read [this section](#installing-wasm-pack-on-a-m1-mac). 

```console
$ wasm-pack build
```

### Running the Development Web Server

```console
$ npm install
```

```console
$ npm start
```

Open http://localhost:8080/ from a web browser.


## Installing wasm-pack on a M1 Mac

As of Jan 2021, wasm-pack project does not provide pre-built binaries for M1 mac.
Use our fork instead.

```console
$ brew install binaryen
$ cargo install wasm-pack --git *TODO* --branch ja-all-fixes
```
